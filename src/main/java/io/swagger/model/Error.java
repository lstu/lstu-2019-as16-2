package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Error
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-20T09:13:54.338Z")

public class Error   {
  @JsonProperty("Num")
  private Long num = null;

  @JsonProperty("tip_error")
  private String tipError = null;

  public Error num(Long num) {
    this.num = num;
    return this;
  }

  /**
   * Get num
   * @return num
  **/
  @ApiModelProperty(value = "")


  public Long getNum() {
    return num;
  }

  public void setNum(Long num) {
    this.num = num;
  }

  public Error tipError(String tipError) {
    this.tipError = tipError;
    return this;
  }

  /**
   * Get tipError
   * @return tipError
  **/
  @ApiModelProperty(value = "")


  public String getTipError() {
    return tipError;
  }

  public void setTipError(String tipError) {
    this.tipError = tipError;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error error = (Error) o;
    return Objects.equals(this.num, error.num) &&
        Objects.equals(this.tipError, error.tipError);
  }

  @Override
  public int hashCode() {
    return Objects.hash(num, tipError);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    num: ").append(toIndentedString(num)).append("\n");
    sb.append("    tipError: ").append(toIndentedString(tipError)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

