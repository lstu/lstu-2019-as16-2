﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ТРПО.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Start_Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Game()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Settings()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

    }
}