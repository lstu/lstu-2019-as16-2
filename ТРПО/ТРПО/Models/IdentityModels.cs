﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;

namespace ТРПО.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
    public class AddObuv
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public float price { get; set; }
        public int razmer { get; set; }
        public string tip { get; set; }
        public string image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}